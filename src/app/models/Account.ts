import { Details } from './Details';

export class Account {
  public id?: number = null;
  public username?: string = null;
  public password?: string = null;
  public details?: Details = null;

  constructor(obj: Account = {} as Account) {
    const {
      id,
      username,
      password,
      details,
    } = obj;

    this.id = id;
    this.username = username;
    this.password = password;
    this.details = new Details(details);
  }

  toString(): string {
    return 'id: ' + this.id
      + ' username: ' + this.username
      + ' password: ' + this.password
      + ' details: ' + this.details;
  }
}
