import { TransLocation } from './TransLocation';

export class Journey {
  public id?: number = null;
  public serialNumber?: string = null;
  public endDate?: string = null;
  public transLocations: Array<TransLocation>;

  constructor(obj: Journey = {} as Journey) {
    const {
      id,
      serialNumber,
      endDate,
      transLocations = []
    } = obj;

    this.id = id;
    this.serialNumber = serialNumber;
    this.endDate = endDate;
    this.transLocations = transLocations;
  }
}
