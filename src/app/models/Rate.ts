export class Rate {
  public id?: number = null;
  public region?: string = null;
  public startDate?: string = null;
  public endDate?: string = null;
  public rateCategory?: string = null;
  public ratePrice?: number = null;

  constructor(obj: Rate = {} as Rate) {
    if (!obj) {
      return;
    }

    const {
      id,
      region,
      startDate,
      endDate,
      rateCategory,
      ratePrice
    } = obj;

    this.id = id;
    this.region = region;
    this.startDate = startDate;
    this.endDate = endDate;
    this.rateCategory = rateCategory;
    this.ratePrice = ratePrice;
  }
}
