import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from './services/auth-guard/auth-guard.service';

import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { VehiclesComponent } from './components/vehicles/vehicles.component';
import { InvoicesComponent } from './components/invoices/invoices.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { RatesComponent } from './components/rates/rates.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'sign-up',
    component: SignUpComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [ AuthGuardService ],
    children: [
      {
        path: '',
        // redirectTo: '/dashboard/(dashboard:vehicles)',
        redirectTo: '/dashboard/(dashboard:invoices)',
        pathMatch: 'full'
      },
      {
        path: 'profile',
        component: ProfileComponent,
        outlet: 'dashboard'
      },
      {
        path: 'vehicles',
        component: VehiclesComponent,
        outlet: 'dashboard'
      },
      {
        path: 'invoices',
        component: InvoicesComponent,
        outlet: 'dashboard'
      },
      {
        path: 'rates',
        component: RatesComponent,
        outlet: 'dashboard'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
