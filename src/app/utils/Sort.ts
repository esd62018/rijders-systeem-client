import moment = require('moment');

import { VehicleHistory } from '../models/VehicleHistory';
import { Invoice } from '../models/Invoice';

export function historySorter(a: VehicleHistory, b: VehicleHistory): number {
  const dataA = moment(a.fromDate, "DD-MM-YYYY");
  const dateB = moment(b.fromDate, "DD-MM-YYYY");

  return dataA.isBefore(dateB) ? 1 : 0;
}

export function invoiceSorter(a: Invoice, b: Invoice): number {
  const dataA = moment(a.invoiceDate, "DD-MM-YYYY");
  const dateB = moment(b.invoiceDate, "DD-MM-YYYY");

  return dataA.isBefore(dateB) ? 1 : 0;
}
