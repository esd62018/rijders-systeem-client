import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpClientService } from '../http-client/http-client.service';

import { Rate } from '../../models/rate';

@Injectable()
export class RatesService {
  private apiUrl = 'rates';

  constructor(
    private http: HttpClientService
  ) { }

  getAllRates(): Observable<Rate[]> {
    return this.http.get<Rate[]>(this.apiUrl);
  }
}
