import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpClientService } from '../http-client/http-client.service';

import { Account } from '../../models/Account';
import { VehicleHistory } from '../../models/VehicleHistory';
import { Vehicle } from '../../models/Vehicle';

@Injectable()
export class VehiclesService {
  private apiUrl = 'vehicle-histories';

  constructor(
    private http: HttpClientService
  ) { }

  getVehicleHistories(userId: number): Observable<VehicleHistory[]> {
    return this.http.get<VehicleHistory[]>(`${this.apiUrl}/${userId}`);
  }

  registerVehicleHistory(vehicleHistory: VehicleHistory): Observable<VehicleHistory> {
    return this.http.post<VehicleHistory>(this.apiUrl, vehicleHistory);
  }

  updateVehicleHistory(vehicleHistory: VehicleHistory): Observable<VehicleHistory> {
    return this.http.put<VehicleHistory>(this.apiUrl, vehicleHistory);
  }

  deleteVehicleHistory(id: number): Observable<VehicleHistory> {
    const url = `${this.apiUrl}/${id}`;

    return this.http.delete<VehicleHistory>(url);
  }
}
