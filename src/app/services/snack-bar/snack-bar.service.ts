import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class SnackBarService {
  private _messageSubject: Subject<string>;
  private _message: Observable<string>;

  constructor(
    private translate: TranslateService
  ) {
    this._messageSubject = new Subject<string>();
    this._message = this._messageSubject.asObservable();
  }

  get message(): Observable<string> {
    return this._message;
  }

  showSnackBar(message: string, delay: number = 2500) {
    this._messageSubject.next(message);

    this.clearSnackBar(delay);
  }

  showOops() {
    this._messageSubject.next(this.translate.instant('ERROR_MESSAGES.WHOOPS'));

    this.clearSnackBar(2500);
  }

  clearSnackBar(delay: number): void {
    setTimeout(() => {
      this._messageSubject.next(null);
    }, delay);
  }
}
