import { NgModule} from '@angular/core';
import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './modules/material/material.module';

import { AppComponent } from './app.component';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { LoaderComponent } from './components/loader/loader.component';
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileFormComponent } from './components/profile-form/profile-form.component';
import { ProfileComponent } from './components/profile/profile.component';
import { VehiclesComponent } from './components/vehicles/vehicles.component';
import { VehicleFormComponent } from './components/vehicle-form/vehicle-form.component';
import { InvoicesComponent } from './components/invoices/invoices.component';
import { InvoiceTableComponent } from './components/invoice-table/invoice-table.component';
import { RatesComponent } from './components/rates/rates.component';

import { HttpClientService } from './services/http-client/http-client.service';
import { AccountService } from './services/account/account.service';
import { VehiclesService } from './services/vehicles/vehicles.service';
import { InvoicesService } from './services/invoices/invoices.service';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { SnackBarService } from './services/snack-bar/snack-bar.service';
import { LoaderService } from './services/loader/loader.service';
import { RatesService } from './services/rates/rates.service';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    SnackBarComponent,
    LoaderComponent,
    LoginComponent,
    SignUpComponent,
    DashboardComponent,
    ProfileComponent,
    ProfileFormComponent,
    VehiclesComponent,
    VehicleFormComponent,
    InvoicesComponent,
    InvoiceTableComponent,
    RatesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    HttpClientService,
    AccountService,
    VehiclesService,
    InvoicesService,
    AuthGuardService,
    SnackBarService,
    LoaderService,
    RatesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
