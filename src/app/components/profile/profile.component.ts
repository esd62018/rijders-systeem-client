import { Component, OnInit } from '@angular/core';

import { AccountService } from '../../services/account/account.service';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';

import { Account } from '../../models/Account';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  auth: Account = null;

  submitButtonText: string = 'Save';

  submitError: string = null;

  constructor(
    private accountService: AccountService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService
  ) { }

  ngOnInit(): void {
    this.auth = this.accountService.auth;
  }

  submit(account: Account): void {
    this.loaderService.showLoader();

    this.accountService
      .update(account)
      .subscribe(
        () => {}, // Ignore
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  onError(error: any): void {
    const { status } = error;

    // TODO: better error handling
    if (status && status === 500) {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    this.loaderService.hideLoader();
  }
}
