import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import moment = require('moment');

import { AccountService } from '../../services/account/account.service';
import { VehiclesService } from '../../services/vehicles/vehicles.service';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';

import { VehicleHistory } from '../../models/VehicleHistory';

import { historySorter } from '../../utils/Sort'

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {
  indexOpenedVehicleHistory: number = -1;

  vehicleHistories: VehicleHistory[] = [];

  submitUpdateError: string = null;

  submitNewError: string = null;

  constructor(
    private accountService: AccountService,
    private vehicleService: VehiclesService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService,
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.getVehicleHistories();
  }

  getVehicleHistories(): void {
    this.loaderService.showLoader();

    const userId = this.accountService.auth.id;

    this.vehicleService
      .getVehicleHistories(userId)
      .subscribe(
        histories => this.vehicleHistories = histories.sort(historySorter),
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  setOpenedVehicleHistory(number: number) {
    this.indexOpenedVehicleHistory = number;
  }

  submitNew(vehicleHistory: VehicleHistory): void {
    this.vehicleService
      .registerVehicleHistory(vehicleHistory)
      .subscribe(
        newVehicleHistory => this.vehicleHistories.unshift(newVehicleHistory),
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  submitUpdate(vehicleHistory: VehicleHistory): void {
    this.vehicleService
      .updateVehicleHistory(vehicleHistory)
      .subscribe(
        updatedVehicleHistory => {
          const index = this.vehicleHistories.findIndex(h => h.id === updatedVehicleHistory.id);

          this.vehicleHistories[index] = updatedVehicleHistory;
        },
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  onTillDateChange(event) {
    const vehicleHistoryId = event.targetElement.name;
    const tillDate = moment(event.value).format('DD/MM/YYYY');

    const vehicleHistory = this.vehicleHistories.find(v => String(v.id) === vehicleHistoryId);
    vehicleHistory.tillDate = tillDate;

    this.submitUpdate(vehicleHistory);
  }

  onError(error: any): void {
    const { status } = error;

    // TODO: better error handling
    if (status && status === 500) {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    // this.filterHistories();
    this.loaderService.hideLoader();
  }
}
