import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { AccountService } from '../../services/account/account.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  constructor(
    private accountService: AccountService,
    private translate: TranslateService
  ) { }

  ngOnInit(): void {

  }

  changeLang(lang: string): void {
    this.translate.use(lang);
  }

  logout(): void {
    this.accountService.logout();
  }
}
