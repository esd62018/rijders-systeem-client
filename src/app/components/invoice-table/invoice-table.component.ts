import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material";
import { TranslateService } from '@ngx-translate/core';
import moment = require('moment');

import { Invoice } from '../../models/Invoice';
import { Journey } from '../../models/Journey';
import { TransLocation } from '../../models/TransLocation';

interface Element {
  position: number;
  dateTime: string;
  distance: number;
  lat: string;
  lon: string;
  previousLat: string;
  previousLon: string;
  price: number;
}

@Component({
  selector: 'app-invoice-table',
  templateUrl: './invoice-table.component.html',
  styleUrls: ['./invoice-table.component.scss']
})
export class InvoiceTableComponent implements OnInit {

  @Input() invoice: Invoice = null;

  tableElements: Element[] = [];

  displayedColumns = ['position', 'dateTime', 'distance', 'from', 'to', 'price'];

  dataSource = new MatTableDataSource(this.tableElements);

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {

    let position: number = 1;
    this.invoice.journeys.forEach(journey => {
      journey.transLocations.forEach(transLocation => {
        if (!transLocation.previousLat || !transLocation.previousLon) {
          return
        }

        const {
          lat,
          lon,
          previousLat,
          previousLon,
          dateTime,
          distance,
          price
        } = transLocation;

        this.tableElements.push({
          position: position++,
          dateTime: moment(dateTime.replace('[UTC]', '')).format('MM/DD/YYYY hh:mm:ss'),
          distance,
          lat,
          lon,
          previousLat,
          previousLon,
          price
        });
      });
    });
  }
}
