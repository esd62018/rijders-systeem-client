import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroupDirective, FormControl, Validators} from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import moment = require('moment');

import { AccountService } from '../../services/account/account.service';
import { VehicleHistory } from '../../models/VehicleHistory';
import { Vehicle } from '../../models/Vehicle';

@Component({
  selector: 'app-vehicle-form',
  templateUrl: './vehicle-form.component.html',
  styleUrls: ['./vehicle-form.component.scss']
})
export class VehicleFormComponent implements OnInit {
  categories: string[] = ['GASOLINE', 'DIESEL', 'ELECTRICAL'];

  @Input() isDisabled: boolean = false;

  @Input() resetAfterSubmit: boolean = false;

  @Input() submitButtonText: string = null;

  @Input() submitError: string = null;

  @Input() vehicleHistory: VehicleHistory = null;

  @Output() parentSubmitCallback = new EventEmitter<Vehicle>();

  brandFormControl = new FormControl('', [
    Validators.required
  ]);

  modelFormControl = new FormControl('', [
    Validators.required
  ]);

  licensePlateFormControl = new FormControl('', [
    Validators.required
  ]);

  buildYearFormControl = new FormControl('', [
    Validators.required
  ]);

  colorFormControl = new FormControl('', [
    Validators.required
  ]);

  carTrackerFormControl = new FormControl('', [
    Validators.required
  ]);

  rateCategoryFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(
    private accountService: AccountService,
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.mapVehicleToFormControlValues();
  }

  mapVehicleToFormControlValues(): void {
    if (!this.vehicleHistory) {
      return;
    }

    const {
      brand,
      model,
      licensePlate,
      buildYear,
      color,
      carTracker,
      rateCategory
    } = this.vehicleHistory.vehicle;

    this.brandFormControl.setValue(brand);
    this.modelFormControl.setValue(model);
    this.licensePlateFormControl.setValue(licensePlate);
    this.buildYearFormControl.setValue(buildYear);
    this.colorFormControl.setValue(color);
    this.carTrackerFormControl.setValue(carTracker);
    this.rateCategoryFormControl.setValue(rateCategory);

    if (this.isDisabled) {
      this.brandFormControl.disable();
      this.modelFormControl.disable();
      this.licensePlateFormControl.disable();
      this.buildYearFormControl.disable();
      this.colorFormControl.disable();
      this.carTrackerFormControl.disable();
      this.rateCategoryFormControl.disable();
    }
  }

  submit(formDirective: FormGroupDirective): void {
    if (
      this.brandFormControl.invalid ||
      this.modelFormControl.invalid ||
      this.licensePlateFormControl.invalid ||
      this.buildYearFormControl.invalid ||
      this.colorFormControl.invalid ||
      this.carTrackerFormControl.invalid ||
      this.rateCategoryFormControl.invalid
    ) {
      return;
    }

    const brand = this.brandFormControl.value;
    const model = this.modelFormControl.value;
    const licensePlate = this.licensePlateFormControl.value;
    const buildYear = this.buildYearFormControl.value;
    const color = this.colorFormControl.value;
    const carTracker = this.carTrackerFormControl.value;
    const rateCategory = this.rateCategoryFormControl.value;

    // When component already contains a vehicle history it means the component is used to update an existing one.
    // In that case, use that vehicle history and create a new vehicle to it (containing the updated values)
    // Otherwise, just create a new vehicle history and vehicle
    let vehicleHistory: VehicleHistory;
    if (this.vehicleHistory) {   // which mean it's being used to update an existing vehicle history
      vehicleHistory = this.vehicleHistory;
      vehicleHistory.vehicle = new Vehicle({
        id: this.vehicleHistory.vehicle.id,
        brand,
        model,
        licensePlate,
        buildYear,
        color,
        carTracker,
        rateCategory
      });
    } else {
      vehicleHistory = new VehicleHistory({
        fromDate: moment().format('DD/MM/YYYY'),
        vehicle: new Vehicle({
          brand,
          model,
          licensePlate,
          buildYear,
          color,
          carTracker,
          rateCategory
        }),
        owner: this.accountService.auth.details
      });
    }

    if (this.resetAfterSubmit) {
      formDirective.resetForm();
      this.brandFormControl.reset()
      this.modelFormControl.reset();
      this.licensePlateFormControl.reset();
      this.buildYearFormControl.reset();
      this.colorFormControl.reset();
      this.carTrackerFormControl.reset();
      this.rateCategoryFormControl.reset();
    }

    this.parentSubmitCallback.emit(vehicleHistory);
  }
}
