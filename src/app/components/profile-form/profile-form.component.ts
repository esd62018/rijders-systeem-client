import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { Account } from '../../models/Account';
import { Details } from '../../models/Details';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {

  @Input() submitButtonText: string = null;

  @Input() showLoginButton: boolean = null;

  @Input() submitError: string = null;

  @Input() account: Account = null;

  @Output() parentSubmitCallback = new EventEmitter<Account>();

  usernameFormControl = new FormControl('', [
    Validators.required
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
  ]);

  firstNameFormControl = new FormControl('', [
    Validators.required
  ]);

  lastNameFormControl = new FormControl('', [
    Validators.required
  ]);

  citizenServiceNumberFormControl = new FormControl('', [
    Validators.required
  ]);

  cityFormControl = new FormControl('', [
    Validators.required
  ]);

  countryFormControl = new FormControl('', [
    Validators.required
  ]);

  zipCodeFormControl = new FormControl('', [
    Validators.required
  ]);

  streetFormControl = new FormControl('', [
    Validators.required
  ]);

  houseNumberFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.mapAccountToFormControlValues();
  }

  mapAccountToFormControlValues(): void {
    if (!this.account || !this.account.details) {
      return;
    }

    const { username, password } = this.account;
    const {
      firstName,
      lastName,
      citizenServiceNumber,
      country,
      city,
      zipCode,
      street,
      houseNumber
    } = this.account.details;

    this.usernameFormControl.setValue(username);
    this.passwordFormControl.setValue(password);
    this.firstNameFormControl.setValue(firstName);
    this.lastNameFormControl.setValue(lastName);
    this.citizenServiceNumberFormControl.setValue(citizenServiceNumber);
    this.countryFormControl.setValue(country);
    this.cityFormControl.setValue(city);
    this.zipCodeFormControl.setValue(zipCode);
    this.streetFormControl.setValue(street);
    this.houseNumberFormControl.setValue(houseNumber);
  }

  submit() {
    if (
      this.usernameFormControl.invalid ||
      this.passwordFormControl.invalid ||
      this.firstNameFormControl.invalid ||
      this.lastNameFormControl.invalid ||
      this.citizenServiceNumberFormControl.invalid ||
      this.countryFormControl.invalid ||
      this.cityFormControl.invalid ||
      this.zipCodeFormControl.invalid ||
      this.streetFormControl.invalid ||
      this.houseNumberFormControl.invalid
    ) {
      return;
    }

    const username: string = this.usernameFormControl.value;
    const password: string = this.passwordFormControl.value;
    const firstName: string = this.firstNameFormControl.value;
    const lastName: string = this.lastNameFormControl.value;
    const citizenServiceNumber: string = this.citizenServiceNumberFormControl.value;
    const country: string = this.countryFormControl.value;
    const city: string = this.cityFormControl.value;
    const zipCode: string = this.zipCodeFormControl.value;
    const street: string = this.streetFormControl.value;
    const houseNumber: string = this.houseNumberFormControl.value;

    const details: Details = new Details({
      firstName,
      lastName,
      citizenServiceNumber,
      country,
      city,
      zipCode,
      street,
      houseNumber
    });

    const account: Account = new Account({
      username,
      password,
      details
    });

    if (this.account && this.account.details) {
      account.id = this.account.id;
      account.details.id = this.account.details.id;
    }

    this.parentSubmitCallback.emit(account);
  }
}
