import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { AccountService } from '../../services/account/account.service';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';

import { Account } from '../../models/Account';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  submitButtonText: string = null;

  submitError: string = null;

  constructor(
    private accountService: AccountService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService,
    private translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.translate.get('SIGN_UP.SUBMIT_BUTTON_TEXT').subscribe(text => {
      this.submitButtonText = text
    })
  }

  submit(account: Account): void {
    this.loaderService.showLoader();

    this.accountService
      .signUp(account)
      .subscribe(
        () => {},    // Ignore (is handled by AccountService)
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  onError(error: any): void {
    const { status } = error;

    // TODO: better error handling
    if (status && status === 404) {
      this.submitError = this.translate.instant('ERROR_MESSAGES.USERNAME_ALREADY_EXISTS')
    } else {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    this.loaderService.hideLoader();
  }
}
