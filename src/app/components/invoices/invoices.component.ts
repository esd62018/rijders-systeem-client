import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material";
import { TranslateService } from '@ngx-translate/core';

import { AccountService } from '../../services/account/account.service';
import { InvoicesService } from '../../services/invoices/invoices.service';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';

import { Invoice } from '../../models/Invoice';

import { invoiceSorter } from '../../utils/Sort'

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {

  invoices: Invoice[] = [];

  indexOpenedInvoice: number = -1;

  constructor(
    private accountService: AccountService,
    private invoiceService: InvoicesService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService,
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.getInvoices();
  }

  setOpenedInvoice(number: number) {
    this.indexOpenedInvoice = number;
  }

  getInvoices(): void {
    this.loaderService.showLoader();

    const userId = this.accountService.auth.id;

    this.invoiceService
      .getInvoices(userId)
      .subscribe(
        invoices => this.invoices = invoices.sort(invoiceSorter),
        error => this.onError(error),
        () => this.onComplete()
      )
  }

  onError(error: any): void {
    const { status } = error;

    // TODO: better error handling
    if (status && status === 500) {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    this.loaderService.hideLoader();
  }
}
