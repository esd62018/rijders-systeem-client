import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import moment = require('moment');

import { RatesService } from '../../services/rates/rates.service';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';
import { AccountService } from '../../services/account/account.service';

import { Rate } from '../../models/Rate';

@Component({
  selector: 'app-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.scss']
})
export class RatesComponent implements OnInit {
  rates: Rate[] = [];

  constructor(
    private ratesService: RatesService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.getRates();
  }

  getRates(): void {
    this.loaderService.showLoader();

    this.ratesService
      .getAllRates()
      .subscribe(
        rates => this.rates = rates,
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  onError(error: any): void {
    const { status } = error;

    // TODO: better error handling
    if (status && status === 500) {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    this.loaderService.hideLoader();
  }

}
